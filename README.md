# Sicurezza dei telefoni per situazioni di attivismo e agitazioni

An Italian translation of [Mobile Phone Security](https://gitlab.com/hakan-geijer/mobile-phone-security) by [Telefoni Sicuri](https://telefonisicuri.noblogs.org/).
This is modified from [the original repository](https://0xacab.org/acabsec/telefonisicuri) to adjust typographical and syntactical errors.

## (Anti-)Copyright

This work is in the public domain under a CC0 license.
