<!-- vim: set spelllang=it : -->

# Tu e il tuo telefono

Il tuo telefono[^1] non è semplicemente un costoso oggetto personale.
È un'estensione della tua persona.
Contiene i tuoi ricordi, le tue conoscenze, i tuoi pensieri privati e semi-privati.
Ti permette di trovare velocemente informazioni e condividerle con altri.
Questa connessione e accesso alla conoscenza ci rende più efficaci nel raggiungimento dei nostri obiettivi.
I telefoni---in un certo senso---sono diventati requisiti necessari per poter funzionare nella nostra società moderna.
Per questa ragione, le persone non ne sono quasi mai prive.
Quando muore la batteria o dimentichiamo il telefono a casa, ci sentiamo nudi, inabili, o come se una parte di noi mancasse.

[^1]: Per comodità, con telefono si intende "telefono cellulare" o "smartphone"

La compromissione di un dispositivo da parte di un avversario, sia essa in seguito a un sequestro o un'infezione di malware, può avere conseguenze disastrose.
Tutte le tue foto, messaggi di testo, email e note potrebbero essere accessibili all'avversario, così come gli accessi a tutti gli account su cui si è temporaneamente loggati dal propio telefono.
Malware o applicazioni per lo stalking potrebbero attivare il microfono dello smartphone o il tracciamento real time dei tuoi spostamenti anche dopo che il telefono ti sia stato restituito.

Oltre a questi tipi di sorveglianza attiva, il tuo telefono fornisce informazioni per la sorveglianza passiva ad avversari con una posizione privilegiata, quali la polizia che può richiedere l'accesso ai metadati posseduti dal tuo ISP[^2] o dalla tua compagnia telefonica, sia real-time che al loro archivio.

[^2]: Internet service provider

È per queste ragioni che gli attivisti dicono "Il tuo telefono è uno sbirro" o "Il tuo telefono è una spia".
Detto questo, dovremmo continuare a usare i nostri telefoni per i vantaggi che il loro uso comporta o dovremmo abbandonarli per i rischi che portano con sé?
Esistono degli accorgimenti su come e quando usare i telefoni che possono permetterci di sfruttare molti dei benefici che comportano, evitando la maggior parte degli svantaggi.
