<!-- vim: set spelllang=it : -->

# Considerazioni finali

La tecnologia non è buona o cattiva---per lo meno una buona parte.
Non significa né emancipazione né oppressione di per sé.
Le nuove tecnologie creano nuove opportunità mentre ne eliminano altre.
Stessa cosa vale per i telefoni.
Avere accesso a comunicazione istantanea e conoscenza illimitata comodamente nelle nostre tasche è spaventosamente potente, ma va di pari passo con un aumento del controllo.

Puoi pensare che lo Stato non ti stia sorvegliando, ma se fai parte dei movimenti---anche nel giro largo---in realtà sicuramente lo fa.
Proteggerti può proteggere i tuoi amici, la tua famiglia, i compagni e le compagne che sono più coinvolte nel movimento.
Puoi pensare che lo Stato stia spiando il tuo telefono per ascoltare le tue assemblee di condominio, ma molto probabilmente non è così.
La sicureza assoluta in ogni momento è irraggiungibile, e provarci è gravoso.

Dopo aver letto queste pagine potrebbe venirti da dire "ma mi tracceranno comunque".
Pensare che non è possibile avere un livello di sicurezza contro le minacce esterne è chiamato nichilismo della sicurezza.
Chi ragiona così di solito prende una di queste due strade: o pensano che nessuna contromisura è efficace, quindi non prendono nessuna precauzione e creano una sorta di profezia che si autoavvera con il proprio arresto; oppure credono nel supremo dominio dello Stato e finiscono paralizzati nell'inattività.
La repressione non funziona solo grazie ai manganelli che ci colpiscono o alle prigioni che ci ingabbiano, ma anche grazie alla paura della sanzione e della nostra conseguente immobilità autoimposta.
Ogni piccolo accorgimento che prendi può proteggerti, e molti di questi sono così semplici che puoi cominciare ad applicarli ora.
Come cosa più facile, puoi evitare la rete della sorveglianza usando delle semplici app di messaggistica crittografate e lasciare il telefono a casa durante manifestazioni e azioni.
Ogni passo oltre questi richiederanno alla controparte uno sforzo maggiore e più mirato se vogliono controllarti o arrestarti.
Il tempo e le risorse sono limitate anche per gli istituti di sorveglianza.
Gli esseri umani fanno errori e i computer si rompono.
I tuoi avversari non sono infallibili e tu puoi diminuire notevolmente la quantità di dati che possono raccogliere e che tipo di conoscenza ne possono trarre.

In più, lo Stato non usa sempre il massimo dei suoi possibili sistemi di controllo.
Solo perchè lo Stato ha l'effettiva possibilità di violare o tracciare il tuo telefono, sicuramente non lo fa per beccarti che passeggi in un parco dopo il suo orario di chiusura.
Anche in casi in cui lo Stato vuole usare al massimo i sistemi di controllo può capitare che lo faccia male.
Il tuo modelo di rischio dovrebbe tenere in conto una risposta verosimile dalla controparte in base alle informazioni che ha su di te.
Prova a capire come si muove la polizia, i fascisti e altri nemici nella tua zona e formula un modello di rischio per te e il tuo giro.
Discutine a lungo con compagni e compagne.
Conincia con un po' di strategie di sicurezza fino a renderle una vera e propria cultura della sicurezza.
Condividi conoscenza e pratiche che portano a un maggiore livello di sicurezza contro i pericoli che è probabile incontrare.
Fai dei passi concreti e tangibili.
Comincia con calma con un paio di cose alla volta finchè vengono interiorizzate come routine, per poi proseguire.
Un piano di questo tipo funziona solo se si mette in pratica, e andare di fretta con un sacco di cambiamenti in un gruppo rischia di essere un sovraccarico eccessivo e portare a frustrazione.
Meglio andare per gradi.

Occhio alle leggende metropolitane. Gli spazi di movimento ne sono pieni, e la sicurezza non fa eccezione.
Chiedi "come?" e "perchè?" quando la gente fa dichiarazioni su controllo e contromisure.
Basa il tuo modello di rischio e i tuoi piani di sicurezza su fatti verificati---o almeno congetture molto realistiche e supportate da qualche tipo di prova.
Usa queste conoscenze per proteggere te e chi ti sta intorno mentre lottate insieme.
