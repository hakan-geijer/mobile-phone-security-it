<!-- vim: set spelllang=it : -->

# La preparazione di un piano

Non possiamo fingere di conoscere il tuo specifico modello di minaccia, né di poter tener conto di tutte le sfumature possibili per ogni situazione o regione geografica.
Quello che possiamo fare è fornire alcune linee guida che sono applicabili in generale.
Nel leggerle, devi valutare tu cosa sia applicabile al tuo caso.
Cosa puoi fare nella pratica? Cosa faranno le persone degli ambienti che frequenti? Il tuo nuovo piano non deve necessariamente essere perfetto, ma deve essere migliore delle tue pratiche attuali.
Può capitare che sia necessario raggiungere dei compromessi su alcuni aspetti della sicurezza al fine di poter continuare ad organizzarsi.
Al tempo stesso, non lasciare che le pratiche poco sicure altrui possano metterti in pericolo.
Trova il giusto equilibrio.

Questa non è in alcun modo una lista esaustiva, piuttosto alcuni metodi per sviluppare una Sicurezza Operativa per sè e migliorare la cultura della sicurezza di gruppo:

- Usa uno smart phone: sono più sicuri rispetto ai semplici telefoni per quanto riguarda la maggior parte delle minacce che gli attivisti devono affrontare.
- Non portare con te il telefono quando partecipi in attività che possano interessare la polizia, in particolare in manifestazioni che possano essere turbolente.
- Scegli applicazioni di messaggistica con crittografia End-To-End (E2EE), abilita l'cancellazione dei messaggi automatica, evita le email.
- Cifra la memoria del tuo dispositivo e imposta una password di sblocco.
- Disabilita l'accesso biometrico (impronta digitale) quando lasci il dispositivo incustodito.
- Fai dei backup regolari delle foto e altri dati su un disco cifrato; poi cancellali dal telefono.
- Cancella i dati vecchi: messaggi, chat di gruppo, email, appuntamenti ecc.
- Esci dalle chat di gruppo dove la tua presenza non è necessaria e cancella i membri inattivi dalle chat di gruppo.
- Allenati a lasciare il telefono a casa o a spegnerlo quando vai a fare delle commissioni o in caso di piccole azioni, in modo da abituarti a non averlo con te.
- Inizia ogni assemblea decidendo se i dispositivi elettronici sono ammessi oppure no; in caso negativo, raccoglili e mettili in un luogo lontano da quello in cui avviene la conversazione.

Soprattutto:

> **Non mandare messaggi o fare chiamate in cui si parli di argomenti particolarmente sensibili.
> Non fotografare o riprendere azioni che possano avere conseguenze legali.
> Non creare delle prove che possano essere usate contro di te o altri.**

## Attenzione all'area geografica

Tutto ciò che hai appena letto, così come il resto della zine, rappresenta delle linee guida.
Potrebbero anche non essere adatte al tuo caso.
In particolare, alcuni accorgimenti di sicurezza digitale possono lasciare delle tracce molto visibili.
Se Signal è molto poco utilizzato nella tua area, il fatto che tu lo utilizzi potrebbe renderti un obiettivo più evidente.
L'uso di VPN potrebbe essere illegale.
L'uso di TOR potrebbe portare a una visita della polizia.
La presenza di applicazioni per la comunicazione sicura sul tuo telefono potrebbero trasformare il tuo arresto in una sparizione.
Prima di scaricare qualsiasi cosa, contestualizzalo con la repressione presente nella tua area per determinare se le linee guida che abbiamo fornito siano adatte alla tua sicurezza o al contrario ti mettano in pericolo.

## Alternative

E' sempre più facile dire "fai questo al posto di quello" invece che "non fare questo" e quando si tenta di cambiare comportamenti e abitudini, fornire alternative aumenta le probabilità che qualcuno effettivamente abbandoni i consueti comportamenti poco sicuri.
Ci sono ottime ragioni per usare dei telefoni, e la presenza di alternative può rendere più facile abbandonare i telefoni o cambiare le proprie abitudini nell'usarli.

Le difficioltà nell'eliminare completamente il telefono sono che le persone vogliono conoscere e raccogliere informazioni, scambiarsi contatti.
Un blocco note e una penna possono essere sufficienti per stilare in maniera analogica i report di assemblee in cui i dispositivi elettronici non sono permessi.
Questo metodo è utile anche per scambiare contatti e addirittura, portando con sé la fingerprint crittografica del proprio dispositivo, si può stabilire una futuro canale di comunicazione sicuro anche quando non si ha con sé i propri dispositivi in quel momento.
Un agenda o un calendario permettono di pianificare eventi.
Stampare mappe cartacee delle aree in cui fare delle azioni può essere utile a orientarsi.
Ricorda solo, in caso si decida di usare carta e penna, bisogna ricordarsi di distruggere ciò che è stato scritto e stampato dopo averlo usato.

## Casi in cui il telefono è inutilizzabile

Oltre a essere utile nel presente, il tuo piano deve anche essere orientato al futuro.
Nel caso in cui, a conoscenza dei rischi, tu abbia deciso di basarti sull'uso del telefono per l'organizzazione, devi considerare che in momenti di
particolare repressione o in caso di catastrofi naturali Internet o i telefoni possano essere inutilizzabili.
È comune che in caso di aumento della repressione lo Stato metta fuori uso le linee telefoniche o Internet per intere aree geografiche.
Se la capacità di organizzazione e sicurezza si basa sul fatto che ogni persona abbia il telefono collegato a Internet, bisogna comprendere che ci si sta esponendo a dei casi in cui questo metodo può fallire.
Il passaparola e il cossiddetto "sneakernet" (corrieri che trasportano dati) sono dei ripieghi e il tuo piano deve prevedere che questi metodi possano a un certo punto essere gli unici validi per comunicare.
