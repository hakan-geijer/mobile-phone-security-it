<!-- vim: set spelllang=it : -->

# Telefoni e tecnologie

Per capire come i telefoni possono essere manomessi e usati per agevolare la sorveglianza, abbiamo bisogno di sapere in maniera precisa in che modo funzionano le varie tecnologie che si usano dentro, come ad esempio cosa c'è dentro (hardware), come ci si parla con quello che c'è dentro (firmware), come farci girare delle app (sistema operativo), e quindi le reti cellulari considerando in generale anche come funziona internet in un telefono.
Questo ci aiuterà a costruire un modello di rischio adatto a noi, così da poter prendere decisioni consapevoli valutando rischi e probabilità.

## Reti Cellulari

Le reti cellulari prendono il loro nome dalla sovrapposizione di molteplici celle fornite dalle torri telefoniche che ricevono e trasmettono il segnale.[^3]
Nelle aree urbane c'è una copertura molto fitta, per cui un singolo telefono è in contatto con più torri.
Nelle aree suburbane e rurali, c'è meno sovrapposizione, di conseguenza un telefono è in contatto con poche torri.

[^3]: non tutte le celle sono torri, ma è sufficente usare il termine generico

Gli operatori di rete possono usare informazioni sul segnale inviato al telefono per stimare la posizione dei dispositivi riceventi.
La posizione approssimativa può essere determinata dall’angolo di arrivo alla torre oppure sapendo da quale settore[^4] arriva il segnale.
Quando la distanza di un telefono è misurata simultaneamente da più torri, il fornitore di rete può triangolare la localizzazione in modo molto accurato.[^5]
Le reti LTE (4G) possono identificare la posizione entro una decina di metri, mentre le 5G hanno una precisione di 5 metri.
Più torri ci sono, più la posizione può essere determinata con accuratezza; di conseguenza, le triangolazioni rurali sono solitamente meno esatte di quelle urbane.

[^4]: L’area a forma di cono coperta da una singola antenna.
[^5]: Questo processo viene chiamato "uplink multilateration".
Da notare che noi usiamo il termine "triangolazione" per indicare "multilaterazione" perché in questo caso abbia senso semplificare un termine per una migliorene la comprensione del processo di localizzazione.

Quando i telefoni si connettono a una rete cellulare mandano un codice identificativo del dispositivo (IMEI[^6]), insieme al loro codice di sottoscrizione (IMSI[^7]).
Un codice IMSI è normalmente memorizzato su una SIM fisica[^8] o su una eSIM[^9].
Ciò significa che scambiare diverse SIM card in un dispositivo oppure sostituire una SIM card con più dispositivi può creare un collegamento fisico evidente tra la vecchia SIM e la nuova SIM con lo stesso telefono.
Per effettuare delle chiamate, non sono richiesti una SIM o un IMSI validi, questi sono richiesti solo perchè è necessaria un autenticazione del dispositivo collegato al carrier ovvero al gestore telefonico che è in grado di determinare se il dispositivo ha il permesso di fare telefonate o usare i dati mobili.
Per esempio, le chiamate d’emergenza possono essere fatte anche senza SIM.
Rimuovere la scheda dal proprio telefono, non previene dall’essere tracciato con il codice identificativo del dispositivo.

[^6]: International Mobile Equipment Identity
[^7]: International Mobile Subscriber Identity
[^8]: SIM: Subscriber Identity Module
[^9]: una scheda SIM integrata, un chip integrato direttamente nel dispositivo

## Tipologie di telefoni

La maggior parte delle persone usa il termine "telefono" riferendosi allo "smartphone", vale a dire quello con un sistema operativo e varie applicazioni che possono essere installate direttamente da chi lo utilizza.
I semplici telefoni (SIMPLE PHONE) invece sono quelli meno sofisticati tra tutti i tipi esistenti, di quelli visti sin dai primi giorni della diffusione dei cellulari: effettuano solo chiamate e inviano sms.
Piuttosto rari al giorno d'oggi i Feature Phones.
I feature phones sono più o meno una via di mezzo tra lo smartphone e un simple phone.
Potrebbero avere app specifiche del produttore (VENDOR) come ad esempio, un programma per le email o un browser integrato per andare su internet.
Per distinguere i feature phones e i basic phones dai più comuni smartphone, useremo il termine “simple phone” per entrambe le tipologie.[^10]

[^10]: Alcune persone utilizzano il termine “dumb phone” per i due tipi di telefoni semplici; stiamo evitando appositamente questa espressione per fare chiarezza.

### Smartphone

Una delle funzionalità degli smartphone è il servizio di geolocalizzazione che permette al telefono di fornire informazioni sulla posizione in tempo reale alle varie applicazioni, prima fra tutte maps.
Il servizio di localizzazione usa segnali ricevuti dai satelliti GPS[^11] o GLONASS[^12] per triangolare la posizione del telefono.
La maggior parte dei cellulari usa il GPS assistito (A-GPS)[^13] che combina i segnali ricevuti dalle torri cellulari, dal WI-FI, ma anche dallo scambio di dati su internet per calcolare velocemente e con maggiore precisione la posizione.

[^11]: Sistema di posizionamento globale, gestito dal dipartimento della difesa statunitense.
[^12]: Sistema globale di navigazione satellitare, un GPS alternativo gestito dall' agenzia spaziale russa Roscosmos.
[^13]: GPS assistito

Gli smartphone spesso sono dotati anche di una bussola, un accelerometro, un giroscopio e un barometro.
Anche senza GPS o multilaterazione, le rilevazioni provenienti da questi sensori possono essere combinate per ottenere la posizione reale utilizzando localizzazioni precedentemente acquisite.
Ciò significa che, nonostante i segnali GPS vengono ricevuti passivamente da un dispositivo, l’utilizzo dei servizi di localizzazione può trasmettere la posizione del telefono.

Inoltre, disattivare la geolocalizzazione potrebbe non essere sufficiente ad evitare che un’app o un malware sul tuo telefono siano in grado di determinare la tua posizione.

### Telefoni Semplici

Molte persone che fanno attivismo credono che usare telefoni non-smartphone sia più sicuro.
In realtà, questa tipologia di telefono può comunque essere geolocalizzata sebbene non abbia il GPS o il servizio di localizzazione; di conseguenza, non offre una protezione significativa nel rilevamento della posizione.
Nei feature phone, in genere, non sono presenti applicazioni di chat vocali o di testo: i telefoni semplice non-smartphone, per definizione, non hanno queste funzionalità.
Ciò significa che si possono solamente effettuare telefonate e mandare sms non codificati; tuttavia sono vulnerabili alle intercettazioni in più modi di quelli che possono andare su internet e possono cifrare i messaggi.
I telefoni non-smartphone, apparentemente i meno avanzati tecnologicamente, arrivano al massimo ad usare il 2G; ciò vuol dire che con appena 25€ di attrezzatura tecnica le telefonate e gli SMS sono facilmente intercettabili.
Inoltre, molti di questi dispositivi posseggono “funzionalità internet” nascoste che inviano i dati telemetrici ai produttori senza che chi li utilizza ne sia consapevole.

In sintesi, facendo riferimento alla maggior parte delle minacce che ogni attivista si trova ad affrontare, possiamo concludere che i cellulari **non-smartphone non sono molto più sicuri degli stessi smartphone**.

### Malware

Un malware è un software dannoso, come un virus.
E’ un app sul tuo telefono che fa qualcosa che tu non vuoi e cerca di essere invisibile.
I malware creati dallo stato, spesso, hanno soltanto il fine di sorvegliare e diffondersi su altri telefoni o persino ad su altri dispositivi elettronici, come il tuo computer o il router wifi di casa.

Diversi siti di prevenzione e sicurezza digitale ci informano che un malware nella maggior parte dei casi viene installato visitando siti web non sicuri o aprendo allegati su email da mittenti sconosciuti; sebbene tutto ciò sia vero, sappi che l’area di attacco del tuo telefono è molto più vasta.
La maggior parte delle app ricevono e inviano messaggi che arrivano da servizi intermediari come Google Play per poi inviare le richieste ai reali server delle applicazioni come Whatsapp.
Alcuni malware hanno un sistema detto “zero-click”, vale a dire che non necessitano di alcuna interazione.
Un esempio è lo spyware Pegasus della NSO (azienda israeliana attiva nel settore informatico, nota proprio per aver inventato Pegasus che consente di sorvegliare da remoto gli smartphone) che ha usato un exploit zero-click e ha preso di mira chi fa attivismo, chi fa giornalismo e varia fauna politica mondiale.

I malware possono essere installati sul tuo telefono anche se usi solo applicazioni sicure e accetti messaggi da contatti affidabili.
Altri  tipi di malware, invece, stanno nella memoria del tuo telefono mentre è acceso e scompaiono dopo un riavvio. Per questo motivo, alcuni malware fingono la routine di spegnimento del telefono per poi eseguire uno spegnimento falso.
Tuttavia, considera che il riavvio e il reset periodico del tuo telefono può  potenzialmente eliminare un malware.

Se pensi che il tuo telefono sia  stato compromesso, avrai bisogno di una persona specialista di malware,  una persona che sia in grado di scoprirlo e, probabilmente, considera  che dovrai procurarti un nuovo cellulare.
I malware sono meno comuni di quanto pensi, ma non far sì che questa informazione ti porti ad ignorare legittimi segnali di avvertimento.
Quelli sponsorizzati dallo stato non saranno facilmente individuabili come gli altri tipi (low-effort malware), dunque i metodi comuni potrebbero non essere applicabili. Sfortunatamente, non è possibile rilevare questi software dannosi in autonomia.

## Sistemi operativi

“Qual è il più sicuro, iOS o Android?”
Questa è una delle domande più comuni che ogni attivista si pone sugli smartphone.

E, come in tutte le domande sulla sicurezza, la risposta è “Dipende”.
I sistemi operativi degli smartphone si dividono in due tipologie: iOS per dispositivi Apple e Android per tutti gli altri.
iOS è brevettato con un codice sorgente privato.
Android è un sistema operativo di base con codice di sorgente pubblica che i produttori possono modificare per i loro dispositivi.
I produttori dei sistemi operativi Android, di solito, hanno un codice sorgente privato. Inoltre, ci sono diverse versioni integrali per Android gestite dalla comunità open source, la più nota è LineageOS [^14].
GrapheneOS and CalyxOS sono sistemi operativi Android open source personalizzati e modificati per avere una notevole attenzione alla privacy e alla sicurezza.

[^14]: LineageOS è il successore del popolare CyanogenMod il cui sviluppo è stato sospeso nel 2016.

Quando un cellulare è acceso, l’hardware inizia a caricare il sistema operativo usando un processo in cui ogni fase verifica l’integrità del software necessario per il passaggio successivo. Questo procedimento è definito in diversi modi come, ad esempio,
secure boot or verified boot (avvio sicuro o avvio verificato).
Per installare un sistema operativo personalizzato, il processo di avvio verificato deve essere disattivato, altrimenti l’hardware rifiuta di caricare il sistema operativo poichè non è crittograficamente firmato da una chiave sicura, la quale era già inclusa dal produttore originale.
Questo comporta che un sistema operativo dannoso, installato al posto di quello autentico, possa leggere i tuoi dati mediante un accesso fisico o un malware.
Tuttavia, ciò non significa che un sistema operativo standard sia più o meno sicuro di uno personalizzato, ma vuol dire che c’è un profilo di rischio diverso quando si disattiva l’avvio sicuro e si utilizza un sistema operativo personalizzato.

Quando il malware viene sviluppato, prende di mira una singola applicazione o il singolo sistema operativo.
Lo sviluppo di malware è molto dispendioso in termini di tempo e denaro; in più, una volta sviluppato può essere rilevato e reso incapace di infettare nuovi dispositivi tramite aggiornamenti dell’app presa di mira [^15].
Di conseguenza, è più economico creare malware che possono colpire più utenti.
iOS ha un numero limitato di versioni per un numero limitato di dispositivi, mentre l’ecosistema Android è variegato e parecchio diverso.
Pertanto, per un avversario è meno economico e più difficile colpire tutti i fruitori di Android.

[^15]: In aggiunta, un malware ha un'interessante caratteristica: quando viene utilizzato può essere copiato e clonato in modo che anche altri possano riusarlo.
E' come se ogni volta che viene lanciato un missile in territorio nemico, ci fosse la possibilità di copiarlo all'istante e riprodurlo all'infinito e che quel modello di missile possa essere intercettato e bloccato anche nel futuro.
I militari sarebbero più titubanti nel lanciare così tanti missili, farebbero attenzione e avrebbero bisogno di essere più strategici nei confronti dei loro obiettivi.

Di seguito le nostre raccomandazioni:

- Per la maggior parte delle persone che provano ad evitare la sorveglianza di massa e gli hacker della domenica/di basso profilo, sono sufficienti sia iOS sia un sistema operativo Android standard, in quanto più facili da usare.
- Per le persone che sono coinvolte in maniera significativa nei movimenti sociali o che credono di essere prese di mira singolarmente, attualmente raccomandiamo per il loro lavoro organizzativo e politico di usare un sistema operativo Graphene senza Google Play, di usare f-droid come unico archivio di app e installare solo un numero minimo di applicazioni richieste per le comunicazioni.
- Per le persone che hanno attirato o pensano di attirare l’attenzione degli apparati investigativi, i telefoni dovrebbero essere evitati per tutto ciò che riguarda l’attivismo.

## Crittografia dei dispositivi

iOS e Android offrono la possibilità di crittografare i tuoi dati personali, sotto il nome di Sicurezza e Crittografia del telefono.
In genere, solo i telefoni più recenti hanno la crittografia del dispositivo di default.
Questa funzione però può essere abilitata dall'utente sia durante la configurazione del telefono che in un secondo momento.
Inoltre è consigliabile abilitare la protezione contro eccessivi tentativi di login sul proprio telefono.

L'attuazione della crittografia dei dispositivi usa solitamente un HSM, ovvero un modulo di sicurezza hardware, sono chip speciali nel telefono che gestiscono i dati crittografati all'interno del telefono[^16].
Questi chip sono importanti poichè proteggono le chiavi da accessi non autorizzati o manomissioni sul tuo telefono; possono impedire alla controparte di accedere ai tuoi dati, se tu non hai dato la tua password, ma non è assicurato che funzioni sempre.
Esiste ad esempio uno strumento chiamato Grey-Key che è in grado di sfruttare i bug dentro dei chip HMS e, in alcuni casi, è stato in grado di decifrare la password di sblocco ed avere quindi accesso ai dati.
Gli HSM che oggi sono sicuri potrebbero avere nuovi bug il mese successivo; la polizia potrebbe sviluppare nuove tecniche per recuperare i dati nei prossimi 5 o 10 anni.
La crittografia del dispositivo fa un buon lavoro impedendo accessi ai tuoi dati nel caso in cui uno spione ottiene l'accesso fisico al tuo telefono o se uno sbirro te lo sequestra durante una perquisizione.
E' improbabile resistere ai tentativi combinati di accesso ai tuoi dati da parte degli apparati investigativi.

[^16]: Sui dispositivi Apple questo chip si chiama Secure Enclave.

Un esempio di grande risonanza è stato quando l'FBI ha decifrato la password del telefono dell'omicida di massa ad un anno dalla sparatoria di San Bernardino del 2015.
Dopo 5 anni dall'accaduto, è stato rivelato che l'accesso ai dati era stato fatto tramite usando dei bug che esistevano nell'HSM.

In generale l'uso della crittografia potrebbe aiutare a poteggersi dall'acquisizione dei dati, ma **l'unico modo per assicurarsi che non finiscano nelle mani sbagliate è far sì che non siano mai esistiti.**

## VPN

Una rete privata virtuale (VPN) si riferisce a un' applicazione che indirizza il traffico internet di un dispositivo a un servizio esterno il cui scopo è solo quello di nascondere il traffico web dell'utente usando un altro indirizzo di rete.
Rendendo molto pià complicato identificare chi sta navigando o controllando le email, la VPN protegge le tue attività sul web nascondendo il tuo indirizzo di rete (IP address).
Quando usata, la VPN permette di nascondere il proprio indirizzo dietro reti wifi pubbliche permettendo di connettersi e navigare di forma più sicura.
 Inoltre, può sviare le indagini e rendere la sorveglianza passiva più difficile, ma potrebbe anche succedere che possa avere dei malfunzionamenti di per se o semplicemente ti dimentichi di attivarlo e facendo così potresti creare un esposizione del tuo indirizzo reale permettendo a chi vuole di identificarti.
Il traffico da e verso il tuo provider VPN può essere collegato agli apparati monitorati dello stato che sono in grado di osservare tutto il traffico internet e, di conseguenza, ogni servizio di VPN è legalmente costretto ad acquisire e cedere i registri delle tuo traffico (logs) alle forze dell'ordine.
Le VPN sono economiche, possono incrementare la sicurezza in vari modi, ma
sarebbe meglio non farci affidamento per garantire l'anonimato contro lo stato.

## IMSI Catchers

Un ricevitore IMSI[^17] è un dispositivo che si comporta come un normale ripetitore telefonico e induce i telefoni vicini a connettersi ad esso, permettendo così l'intercettazione o l'invio di SMS o chiamate.
A volte, la manomissione (spoofing) è rilevabile, ma non sempre è facile individuarli.
In alcune regioni, questi ricevitori possono essere impiegati senza mandato, soprattutto durante le manifestazioni.
In parte, lavorano diminuendo il protocollo ad uno senza crittografia o con una crittografia fragile così da poter intercettare la conversazione.
Sebbene gli smartphone abbiano impostazioni per protocolli che offrono una maggiore protezione contro intercettazioni e manomissioni, è possibile far sì che i telefoni funzionino anche solo con il 2G che fa parte del GSM standard, è quindi possibile essere declassati per usare i protocolli non sicuri dei ricevitori IMSI.
Usando questa tecnica le telefonate e i messaggi inviati e ricevuti dagli smartphone non resistono alle intercettazioni dei ricevitori IMSI.

[^17]: Spesso i ricevitori IMSI vengono chiamati StingRay, nome di una nota azienda di ricevitori

## Borse di Faraday

I telefoni mandano e ricevono informazioni usando radiazioni elettromagnetiche, le quali possono essere bloccate da materiali speciali.
Leggende metropolitane e alcuni elementi di prova affermano che i segnali possono essere bloccati mettendo il telefono in uno o più sacchetti di patatine[^18], i quali hanno una pellicola interna, ma questa, così come molte altre contromisure, non dovrebbe essere presa seriamente in considerazione.
Può essere aquistato o costruita una gabbia o borsa di Faraday e farci affidamento per bloccare i segnali telefonici.

<!-- TODO footnote makes no sense in italian -->
[^18]: anche conosciute come buste di alluminio o buste delle patatine dagli yankee.

Se hai bisogno di portarti dietro il telefono ma vuoi avere la sicurezza che non trapeli nessun tipo di segnale, allora spegnerlo potrebbe non essere sufficiente.
Sono pochi gli smartphone a cui è possibile rimuovere la batteria.
Qualcosa che poggia sul telefono nella tua borsa potrebbe premere il bottone di accensione.
I malware possono fingere lo spegnimento e impedire al telefono di spegnersi realmente mentre stai provando a farlo.
Mettere telefoni spenti in una borsa di Faraday può evitare che il telefono mandi segnali ed evitare considerevolmente la possibilità di stabilire il tuo segnalo o la tua posizione.
