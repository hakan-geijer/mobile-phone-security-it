<!-- vim: set spelllang=it : -->

# Le basi della sicurezza

Ci sono alcune pratiche legate all'uso dei cellulari che sono consigliabili per la maggior parte delle persone che vogliono fare attivismo.
Alcune di queste consigli e pratiche sono riportate di seguito.

## Aggiornamenti

In linea generale, la prima cosa che si può fare per prevenire intrusioni da parte delle forze dell'ordine---o di hacker a caso---è installare gli aggiornamenti del sistema operativo dello smartphone e di tutte le app.
Può essere una scocciatura, ma molti aggiornamenti contengono delle patch di sicurezza per vulnerabilità critiche.
Se non altro, ciò può evitare che i tuoi account bancari e di pagamento vengano prosciugati.

## Password Manager

La seconda pratica più utile e generalmente applicabile è usare un gestore di password (Password Manager) per tutti i tuoi account, inclusi quelli sul tuo telefono.
Ce ne sono versioni a pagamento che permettono la sincronizzazione automatica delle password tra dispositivi e il login automatico alle pagine web.
Tuttavia, questi richiedono un certo livello di fiducia nell'azienda che offre il prodotto.
Esistono alternative gratuite come [KeePassXc](https://keepassxc.org)[^19] che però non hanno la stessa facilità d'uso dei prodotti a pagamento.
Quando si usa un Password Manager, tutti i tuoi account dovrebbero avere password forti, uniche e casuali, che comunemente vengono generate in automatico dal programma stesso.
La password "master" per sbloccare il programma dovrebbe essere una frase lunga e casuale.

[^19]: https://keepassxc.org

Le persone sono notoriamente pessime nel generare buone password e anche usando il verso iniziale del tuo poema preferito o qualche articolata sostituzione di caratteri per trasformare `antifascismo` in `4n7if4sc1sm0!` il che può far pensare di rendere le password difficilmente crackabili da parte dei computer, ma non è così.
Il "diceware" è un metodo di creazione delle password che lancia un immaginario dado e utilizza i numeri ottenuti per selezionare delle parole all'interno di una lista predefinita.
Cinque parole è il minimo assoluto, sei è meglio, mentre qualsiasi combinazione oltre le otto parole è persino esagerata.
Fare questo permette di ottenre una casualità impossibile da indovinare, che non si potrebbe mai ottenere autonomamente, inoltre una frase è facilmente memorizzabile.
Una lista di termini in inglese facilmente utilizzabile è fornita dall'EFF (Electronic Frontier Foundation).
Una frase di esempio è CambiaCalmaBollaRestaGiusta (si prega di non utilizzarla; creane una tua).

: Parole di esempio dell'EFF per il "diceware"

+-------------+---------------+--------------+
| **Numeri**  | **EFF**       | **Italiano** |
+=============+===============+==============+
| `24311`     | drowsily      | divine       |
+-------------+---------------+--------------+
| `24312`     | drudge        | divini       |
+-------------+---------------+--------------+
| `24313`     | drum          | divino       |
+-------------+---------------+--------------+
| `24314`     | dry           | divisa       |
+-------------+---------------+--------------+
| `24315`     | dubbed        | divise       |
+-------------+---------------+--------------+
| `24316`     | dubiously     | divisi       |
+-------------+---------------+--------------+
| `24321`     | duchess       | divora       |
+-------------+---------------+--------------+
| `24322`     | duckbill      | divori       |
+-------------+---------------+--------------+

## Blocca il tuo telefono

A seconda della minaccia, potresti voler rendere difficile se non addirittura impossibile sbloccare il tuo telefono.
Questo perché il metodo di sblocco è anche il metodo di decrittazione, per cui un metodo di sblocco forte aiuta a difendersi da accessi indesiderati ai tuoi dati se il tuo telefono viene preso.
In generale, dovresti preferire le Password ai PIN e ai tracciati di sblocco telefono, dal momento che le passowrd sono più difficili da crackare.
Dovresti sicuramente anche disabilitare e non usare lo sblocco facciale e preferibilmente anche lo sblocco tramite impronta digitale.
In alcune regioni vi sono protezioni giuridiche per le password, ma non per le impronte o per altri dati biometrici.

Alcuni telefoni offrono la possibilità di cancellare tutti i dati sul telefono se avvengono troppi tentativi errati di sblocco.
Ha senso valutare di attivare questa funzionalità se possibile e tenere il telefono lontano da persone moleste, piccole creature curiose e animali domestici.
Si consiglia anche di disabilitare le notifiche nella schermata di blocco o, perlomeno, disattivare quelle delle app di messaggistica che contengono informazioni sensibili.
Se hai la crittografica abilitata sul dispositivo, i tuoi dati sono protetti molto meglio quando il telefono è spento (o è stato riavviato, ma la tua password di sblocco non è stata ancora inserita).
Se hai attivato la cifratura considera che dopo aver sbloccato il telefono al riavvio, la protezione dei tuoi dati è molto minore rispetto a quando è spento.

Molti attivisti lasciano lo sblocco con impronta abilitato perché è molto più pratico rispetto a dover digitare una password da 30 o più caratteri 100 volte al giorno.
Dal momento che la praticità spesso ha la meglio sul bisogno di sicurezza, ciò rappresenta un'ulteriore ragione per considerare di non tenere dati sensibili sul proprio telefono.
Comunque, venendoci incontro, se hai lo sblocco con impronta attivato puoi disabilitarlo temporaneamente configurando un app che tenendo premuto il pulsante di accessione disattivi lo sblocco con
impronta digitale.
Puoi farlo prima di avere interazioni con le forze dell'ordine, prima di andare a dormire o prima di lasciare il tuo telefono incustodito.

## Funzionalità wireless

Un altro consiglio pratico è quello di disabilitare Wi-Fi e Bluetooth quando non li stai usando.
Entrambi possono essere utilizzati per identificare il tuo telefono.
In più, aumentano i potenziali punti di attacco da parte di chi tenta di entrare nel tuo telefono.
Sebbene i rischi di lasciarli sempre attivi sia minimo, queste pratiche contribuiscono in una certa misura alla tua sicurezza e quindi, se non li usi, perché non farlo?

## Backup

Gli smartphone spesso includono delle funzionalità di backup automatico su un account cloud legato al telefono (iCloud per Apple e Google Drive per Android).
Tempo fa, Apple ha sospeso i suoi piani di garantire backup criptati su iCloud a seguito delle pressioni dell'FBI, per cui i backup sui dispositivi Apple risultano non-criptati.
Google, invece, offre una crittografia end-to-end dei backup che, secondo indagini esterne, fornisce notevoli garanzie di privacy nei confronti di Google stessa e delle forze dell'ordine.
In più, alcune app possono avere i loro servizi di crittografia.
Ad esempio, è importante notare
che WhatsApp può fare il backup delle tue conversazioni sui suoi server il che
rende molto meno sicuro l'uso di quest'app per le comunicazioni.

La nostra raccomandazione è di evitare i backup su Apple, mentre i backup su Google sono sicuri nella misura in cui non vi siano comunque prove incriminanti sul tuo telefono.
Considerato che i dati che vengono inviati a terze parti potrebbero essere persi o distrutti anche se non vengono recuperati dalla polizia, dovresti prendere in cosiderazione di fare un backup periodico su un hard disk cifrato che tieni a casa o in qualche posto sicuro.

## App di messaggistica

Le app di messaggistica rappresentano un'alternativa più sicura rispetto alle chiamate e agli SMS.

### Crittografia

Le app di chat testuale e vocale offrono uno o due tipi di crittografia.

Si ha **crittografia client-server** quando il canale tra un client (es. il tuo telefono) e il server è criptato e protetto da intercettazioni o manomissioni.
Il messaggio è decifrato e salvato sul server.
Quando il messaggio è richiesto da un altro client (es. il telefono del tuo amico), viene nuovamente cifrato e inviato.

**La crittografia end-to-end** (E2E) prevede che i client generino chiavi crittografiche e si scambino le chiavi pubbliche tra loro.
I messaggi sono cifrati usando la chiave pubblica del client di destinazione e inviati tramite il server, che agisce come un ripetitore cieco, in quanto il messaggio è decifrabile solo dall'altro client.

La crittografia E2E fa si che un server o entità esterno tra il tuo telefono e quello della persona con la quale stai comunicando non possa leggere o alterare il messaggio inviato.
Un agente malevolo può però ottenere informazioni dai metadati relativamente alla dimensione del messaggio, all'orario di invio, al mittente e al destinatario.

Alcune app di messaggistica offrono crittografia E2E ad attivazione, come Telegram con le sue chat segrete, ma questa funzionalità non è disponibile per le chat di gruppo.
Altre app come Signal o Wire impiegano tassativamente la crittografia E2E, così come iMessage (Apple) e WhatsApp.[^20]
Alcune app come Element hanno la crittografia E2E abilitatà di default, ma può essere disattivata per garantire la compatibilità con i client più vecchi.

[^20]: Ci sono altre app di chat peer-to-peer rilevanti come Biar e Cwtch, che hanno metadati molto resistenti e possiedono altre funzionalità di sicurezza interessanti, ma che sono scarsamente utilizzate.
Sono anche disponibili per iOS, il che spinge molte persone a non usarle per le comunicazioni sicure.

La sicurezza della crittografia E2E dipende dalla verifica delle chiavi scambiate, che è spesso effettuata scansionando un QR code contenente la fingerprint che è unica e identifica la tua chiave di cifratura generata.
Alcune app richiedono di verificare solo una fingerprint per tutti i dispositivi.
Altre inviano una notifica nella conversazione quando l'impronta digitale del tuo contatto cambia, suggerendo un cambio di telefono o qualcosa di strano per cui è meglio verificare se è avvenuto un cambio di telefono o una reinstallazione.
Altre app sfortunatamente non verificano questo cambio.

Consigliamo quindi di verificare tutte le impronte digitali di tutti i dispositivi e se per caso una cambia, dovresti riverificarla, altrimenti tutta la tua sicurezza potrebbe essere a rischio di qualche altra persona che sta usando il numero del tuo amico con un altro telefono.
Inoltre, alcune app di chat non condividono i dati (messaggi e media) se non hai verificato la fingerprint con gli altri dispositivi nello stesso gruppo, per condividere i dati bisogna di verificare la fingerprint di tutti i dispositivi dei contatti appartenenti al gruppo.

![QR Code e impronte digitali](img/svg/qr-code.svg){#fingerprints-and-qr-codes width=80% }

### Utilizzo

Il mantra "usa solo Signal" è spesso ripetuto da ogni attivista, ma presuppone erroneamente che tutte le persone siano soggette alle stesse minacce.
In alcune regioni, l'uso di Signal può essere bloccato dai firewall di Stato e il suo uso può essere talmente raro che un utente risulti automaticamente sospetto.
In Nord America ed Europa, questi problemi generalmente non ci sono.
Tuttavia, vi sono lamentele diffuse nei confronti di Signal come il fatto che necessiti di un numero di telefono per registrarsi e che la lista di contatti sia condivisa con il server in modi non troppo sicuri, che permettono di scoprire i contatti e lo scambio iniziale delle chiavi.

Per la maggior parte delle app, quando i messaggi sono ricevuti, vengono decifrati e salvati in chiaro sul dispositivo.
Alcune app, come Signal, permettono di impostare una password per prevenire l'acceso ai messaggi mentre qualcun altro sta usando il telefono, ma ciò non cifra doppiamente i messaggi in alcun modo.
Se la crittografia del dispositivo è abilitata, puoi riottenere un po' di privacy su quei messaggi, come descritto nella sezione dedicata alla crittografia.

Dal momento che i messaggi sono salvati in chiaro e possono essere recuperati anche con la crittografia del dispositivo, potresti ragionevolmente voler abilitare i messaggi temporanei (a scomparsa).
Su alcune app, qualcuno può abilitare i
messaggi temporanei per tutti i membri della chat.
Su altre app, ogni persona deve attivare la funzionalità per assicurare che i messaggi scompaiano effettivamente dopo il tempo stabilito.
Anche se puù essere scomodo avere messaggi temporanei, dato che immagini, file o messaggi sono disponibili solo
per l'ultima settimana o mese, ma è sicuramente preferibile rispetto all'avere
un registro pluriennale di tutto ciò che hai detto o pensato e, in particolare, di tutti i posti che dici di aver attraversato.

Questo significa che dovresti decisamente preferire app di chat testuali e vocali che utilizzano tassativamente la crittografia E2E, a meno che non ci sia una ragione di sicurezza stringente per non farlo, dovresti verificare le chiavi prima di messaggiare e dovresti probabilmente abilitare i messaggi temporanei.

### Non basta usare solo Signal

Diverse organizzazioni per la privacy e che fanno attivismo e si occupano di questi temi ogni giorno hanno fatto un lavoro eccellente nell'incoraggiare l'adozione di Signal tra le persone comuni e specialmente con ogni attivista.
Hanno fatto un lavoro forse fin troppo buono, al punto che molte persone l'hanno interpretato come "se usi Signal, sei totalmente al sicuro".
Questo ha portato alcune persone a discutere di questioni di cui non dovrebbero assolutamente parlare utilizzando dispositivi elettronici, assumendo erroneamente di essere a posto perché usano Signal.
Tutte le contromisure di sicurezza si basano su di un insieme di assunti, da cui possono derivare dei rischi espliciti o cose che sono fuori ambito.
Signal è piuttosto buona nel prevenire che gli attori statali usino la sorveglianza di massa per leggere i contenuti dei messaggi di testo.
Nasconde persino alcuni---non tutti---metadati.
Altre app di chat hanno modelli di rischio pressoché uguali.
Tuttavia, se il tuo telefono è compromesso da un malware perché hai attirato attenzioni indesiderate o semplicemente perché la fortuna non è dalla tua parte, Signal non impedirà che le tue comunicazioni possano essere lette[^21].

[^21]: Inoltre, alcuni hanno adottato pratiche di sicurezza assurde, come entrare in varie chat di gruppo su Signal e discutere delle propore azioni senza accertarsi di chi fossero le altre persone nel gruppo.
Non importa quanto buona sia la crittografia se una persona all'interno del gruppo è un infiltrato o una spia, non sei al sicuro.

![Metodo di input e risultati in Pinyin](img/bin/ime-pinyin-options.png){#input-method-editor width=50%}

Per alcune lingue, in particolare quelle basate sui caratteri e non sulle lettere, gli editor del metodo di input (IME) vengono usati per convertire sequenze di lettere dell'alfabeto latino nei caratteri della lingua di destinazione.
Questo viene fatto generalmente da app di terze parti installate sul dispositivo.
Signal non è stata in grado di avvisare adeguatamente gli utenti che utilizzano gli IME sulla possibilità che le loro chat possano essere lette dal software stesso e riportate allo Stato prima che i messaggi siano cifrati.

**Signal non è garanzia di sicurezza.**
Lo stesso vale per qualsiasi altra app di messaggistica che utilizza la crittografia E2E. Non le considerate tutte uguali.

Sebbene vi siano forti critiche nei confronti di Signal, questo è dovuto
alla sua popolarità e ai fraintentimenti sul suo riguardo. Al momento in cui si
scrive, è ancora una di un ristretto numero di app di chat cifrate che offrono
un buon livello di sicurezza.

## E-mail

Ci sono modi per rendere le comunicazioni via e-mail più sicure, ma l'e-mail come protocollo e mezzo di comunicazione è tendenzialmente poco sicura per le comunicazioni private.
Gestori e-mail di nicchia e vicini alle esigenze di chi fa attivismo (es. autistici.org, riseup.net, ecc.) offrono significativi benefici di sicurezza contro le intercettazione da parte delle forze dell'ordine o hacker.
Quando mandano le e-mail, alune persone utilizzano PGP o S/MIME, ma sono difficili da usare e non hanno una buona esperienza utente.
Due persone che utilizzano questi mezzi di crittografia possono avere una protezione piuttosto buona contro le intercettazioni, ma un click sbagliato può inviare l'intera cronologia della conversazione in chiaro, rendendola leggibile dalle forze dell'ordine.
ProtonMail ha fatto annunci audaci sulla crittografia per le sue e-mail e i suoi client e molte persone hanno inteso queste mezze verità come se usare un account ProtonMail significasse che tutte le proprie e-mail fossero cifrate, ma non è questo il caso.

Le e-mail dovrebbero essere tendenzialmente evitate per pianificazioni e in particolare per comunicazioni sicure.
Detto questo, l'e-mail rimane popolare perché ogni dispositivo può ricevere e inviarle e alcune persone non vogliono o non usano smartphone.
Per coordinare associazione degli affittuari o stabilire i turni al punto informazioni locale, le e-mail possono andare bene.
Se scegli di usare le e-mail, parti dal presupposto che le forze dell'ordine stanno leggendo tutti i messaggi e mantieni la conversazione al minimo.
Non trattare di attività illegali.
Non discutere di dettagli succosi che possono essere sfruttati dallo Stato.

Infine, ci sono casi legittimi dove le e-mail e PGP possono essere l'ultima risorsa come un canale criptato utilizzato una tantum da qualcuno in fuga, in modo da stabilire un secondo canale di comunicazione più sicuro.
In casi come questo, i telefoni dovrebbero essere evitati per la loro facile tracciabilità.

## Più profili, più telefoni

A seconda del tuo modello di minaccia, potresti scegliere di mantenere più telefoni collegati a diversi account.
Ad esempio, potresti avere un telefono per le tua vita pubblica, con gli account dei social media che usi per sentire la tua famiglia e un secondo telefono con una SIM separta e account distinto legato alla tua vita da attivista.
Questa separazione degli account è parte di un processo chiamato compartimentalizzazione.

Il primo beneficio è che l'uso di diversi dispositivi per ciascuno dei tuoi profili previene che errori di programmazione o errori dell'utente possano far trapelare le tue informazioni private.
Le app sul tuo telefono potrebbero avere comportamenti imprevisti come ad esempio mandare alla tua intera lista di contatti una richiesta di collegamento quando ti registri su un app di messaggistica.
Potresti fare l'errore di rispondere a un post sui social media dall'account sbagliato.
Quando clicchi su un indirizzo e-mail con l'intezione di usare uno dei tuoi alias, il sistema operativo del tuo telefono potrebbe iniziare a comporre l'e-mail con un client di default legato ad un altro alias e far scoprire un altra identità.

Il secondo beneficio è che il tuuo dispositivo da attivista può essere minimale e usato esclusivamente per comunicazioni sicure.
Ogni app che installi è un possibile canale di ingresso per un malware, per cui se il tuo telefono ha solo il sistema operativo e due app di chat, è più difficile da compromettere.
Usare molteplici telefoni di per sé non impedisce alle forze dell'ordine di collegare i tuoi account.
Se porti con te i telefoni nello stesso momento o li usi negli stessi luoghi, possono essere collegati tra loro.

Come alternativa all'uso di più telefoni, puoi ridurre parte del rischio di fuoriuscta di dati a causa di errori o comportamenti imprevisti creando molteplici profili sul tuoi dispositivo Android.
Questo non ti proteggerà dai malware, ma ti fornirà un po' di protezione.
Uno dei casi più comuni per cui si hanno più telefoni è per organizzare un sindacato.
Alcune aziende prevedono che le app siano installate da remoto come mezzo per proteggere la proprietà intellettuale o per mitigare le falle nella sicurezza.
Queste sono app spyware e possono controllare completamente il tuo telefono.
A parte questi casi, molte società impongono un'app di chat per le comunicazioni.
Dovresti evitare di organizzare attività sindacali sui dispositivi aziendali o su quelli con spyware aziendali installati e dovresti evitare di utilizzare la chat aziendale per attività di sindacalizzazione.

## Telefoni usa e getta, demo e prepagati

Molte persone comprendono l'importanza dei loro telefoni e sanno che possono essere tracciati tramite essi o che la loro perdita può essere devastante.
Vi sono diversi approcci utilizzati da chi fa attivismo per aiutare a ridurre il rischio anche se non sono in grado di spiegarlo completamente o non sanno perché le loro contromisure funzionino.
Alcune persone hanno telefoni demo o prepagati che si portano quando agiscono o quando oltrepassano il confine.
Questi dispositivi hanno dati privati minimi e sono considerati inaffidabili, a causa della possibilità di poterci installare un malware, quando in mano alle forze dell'ordine.
Questi telefoni non sono usati per l'anonimato.
Possono condividere una SIM card con quella utilizzata tutti i giorni e possono essere usati in un modo che li geolocalizzi nella residenza dell'utilizzatore.
I telefoni demo forniscono pochi dati su messaggi o uso dell'account alla polizia nel caso vengano sequestrati.
Non ci sono vincoli sul fatto che i telefoni demo e prepagati siano telefoni semplici.
In molti casi sono smartphone perché questo permette agli utenti di avere mappe e comunicazione criptata E2E.

Chi fa attivismo erroneamente utilizza la formula "telefoni usa e getta" per
descrivere i telefoni demo, i prepagati o qualsiasi telefono semplice.[^22]
Un telefono usa e getta prende il nome dal fatto che può essere utilizzato una sola volta, dopodiché di distrugge.
Sono acquistati quando chi li usa ha bisogno di avere molteplici comunicazioni durante il corso dell'azione che porterà a un'indagine investigativa su larga scala.
Perché un telefono sia usa e getta deve rispettare i seguenti criteri:

[^22]: Sembra che la gente utilizzi la formula "telefono usa e getta" perché suona "mega illegale" e "super criminale" e non perché stanno descrivendo le proprietà di un telefono usa e getta.

1. Il telefono deve essere acquistato in contanti[^23].
1. La SIM card usata deve essere stata pagata in contanti.
1. Il telefono e la SIM card devono essere stati comprati da una persona che non ha altri telefoni o dispositivi tracciabili sulla sua persona al momento dell'acquisto.
1. Il telefono e la SIM card devono essere usati solo in combinazione tra loro.
1. Il telefono non deve mai essere portato in luoghi associati con l'utilizzatore a meno che non sia contemporaneamente spento e in un una gabbia di Faraday.
1. Il telefono non deve essere mai usato in presenza di telefoni non usa e getta o altri dispositivi che possono essere ricondotti all'utilizzatore o ai suoi associati.
1. Ogni account sul telefono deve essere fatto anonimamente, usato solo da quel dispositivo e mai più.
1. Il telefono deve essere usato per una e una sola azione.
1. Il telefono deve contattare solo altri telefoni usa e getta o parti non affiliate (es. un ufficio o l'obiettivo dell'azione).
1. Il telefono e la SIM card devono essere disattivati al termine dell'azione e distrutti immediatamente.

[^23]: Il furto di un telefono con una SIM attivata non è generalmenete consigliato perché ogni furto crea un data point di localizzazione aggiuntivo che può essere collegato all'azione, il telefono potrebbe non essere sbloccato e chi possiete il telefono potrebbe aver inserito il dispositivo nella lista nera gestita dagli operatori per evitare che riceva o effettui chiamate o utilizzi i dati mobili.

Un elemento che complica le cose è che alcuni telefoni o SIM card richiedono un'attivazione tramite chiamata a un numero o accedendo al sito del provider.
In alcuni casi, questi siti bloccano le connessioni Tor.
Usare una telefono non usa e getta per attivare la SIM card è una palese violazione del protocollo di sicurezza.
Potresti avere bisogno di trovare un telefono pubblico o convicere un estraneo alla stazione dei treni a prestarti il suo telefono per qualche minuto.
Quando diciamo che un telefono usa e getta può essere usato per una sola azione, intendiamo "una sequanza di attività in un lasso di tempo limitato".
Questo può significare un'azione diretta che ha luogo in sole due ore.
Può anche significare la pianificazione e il coordinamento nel mese precedente all'azione così come durante l'azione stessa.

Con un utilizzo particolarmente attento, un gruppo chiuso può riutilizzare i suoi telefoni usa e getta per azioni ricorrenti.
In questo caso, i telefoni devono essere usati in lotti cosicché i cicli di utilizzo dei dispositivi non si sovrappongano tra di loro.

Un fattore non vincolante, ma caldamente consigliato è che il telefono non sia acquistato immediatamente prima dell'azione.
Questo crea possibilità aggiuntive che eventuali filmati di sicurezza del luogo di acquisto siano disponibili per le forze dell'ordine.

Cercare di nascondere l'esistenza del ciclo chiuso tra i telefoni può aiutare a impedire l'identificazione del gruppo.
Un passo è non attivarli tutti in un breve lasso di tempo.
L'attivazione graduale è meno riscontrabile quando lo Stato analizza i dati.
Fai alcune chiamate da luoghi casuali a numeri che qualcuno chiamerebbe di solito, ma non parlare se qualcuno risponde.
Chiama numeri con tempi di attesa ragionevolemente lunghi come banche o compagnie di assicurazione.
Chiama un po' di negozi locali prima che aprano o dopo la chiusura.
Queste false telefonate potrebbero non essere necessarie, dal momento che molti utenti in certe regioni non effettuano mai chiamate e utilizzano i loro piani dati per tutto.
Data l'attenzione con cui un telefono usa e getta deve essere acquistato e usato, è altamente improbabile che valga gli sforzi.
Se pensi che la tua azione necessiti di un telefono usa e getta, dovresti quasi certamente provare a cercare un modo per compiere l'azione senza alcun telefono.
Per aiutare a chiarire ad ogni persona che un telefono usa e getta deve avere queste proprietà, evita di chiamarlo telefono usa e getta o e utilizza il termine demo o prepagato, se possibile.

## Riduzione controllata

Questa zine tratta principalmente di caratteristiche ideali per un utilizzo sicuro del telefono, ma spesso questi ideali non sono raggiungibili.
Un esempio di questo si ha se ti stai organizzando con persone che non si possono permettere uno smartphone.
Comprare dei telefoni economici per una rete di persone che vogliono organizzare un'azione o anche semplicemente per coordinare degli incontri periodici può essere più facile e più gestibile finanziariamente che fare lo stesso con degli smartphone. Sfortunatamente, la mancanza di app di chat cifrate implica una sorveglianza più stringente per i tuoi messaggi.

Per evitare che lo Stato ottenga troppe informazioni sulle tue azioni, conviene affidarsi a soluzioni umane anziché tecniche.
Un accordo per discutere esclusivamente degli orari e dei luoghi di incontro con un quantitativo essenziale di informazioni può ridurre le informazioni raccolte al minimo.
Un semplice cifrario che rimpiazzi frasi comunemente utilizzate nell'organizzazione con frasi in codice casuali e innocue può evitare che i sistemi automatizzati allertino le autorità.
Usare soluzioni di questo tipo può permettere di ridurre gradualmente da alti livelli di sicurezza e livelli più bassi senza esporsi completamente alla sorveglianza e alla repressione di Stato.
Questi metodi richiedono grande attenzione, ma sono possibili.
