<!-- vim: set spelllang=it : -->

# Introduzione

Alcuni dei più importanti strumenti a nostra disposizione sono i nostri telefoni sempre connessi ad internet.
La comunicazione istantanea e la somma di tutta la conoscienza umana a disposizione di un tocco ha incredibilmente cambiato la possibilità di influenzare e farci influenzare dal mondo.
Ma questa costante connessione ha un prezzo, e lo paghiamo con il costo della crescente sorveglianza degli Stati e delle aziende private di sorveglianza.
Coloro che sono attivi in movimenti libertari sono a conoscenza dei vari livelli di sorveglianza e collettivamente hanno sviluppato un modello di sicurezza operazionale (OpSec), ovvero un insieme di pratiche e cultura alla sicurezza per contrastare i tentativi di chi vuole ostacolare un' organizzazione.

Ci sono molte leggende metropolitane sui cellulari che provengono da un' incompleta comprensione della tecnologia usata dai telefoni alla base e quali caratteristiche lo stato e i settori privati hanno a disposizione per sorvegliare le persone usando i loro telefoni.
Il Modello di rischio è il processo base per identificare le minacce e costruire contromisure specifiche e pragmatiche contro questi avversari, ma senza un modello chiaro di funzionamento di tali avversari si rischia di usare contromisure inefficaci.
Azioni basate sulla disinformazione possono portare ad arresti immediati o creare l'impressione di un avversario che vede tutto, soffocanto così l'azione.
Questo testo prova a coprire le tecnologie alla base di un telefono e sfatare certe leggende metropolitane così che tu e le persone tue complici possiate resistere e organizzarvi efficacemente.

Non esiste una cosa come la sicurezza perfetta, non è una cosa binaria che si può "accendere" o "spegnere", non esiste nemmeno uno spettro di "miglior sicurezza" o "peggior sicurezza". La sicurezza ha senso se discussa in termini di "miglior sicurezza sotto certe condizioni con problemi e minacce particolari".
Quello che può essere efficace per evitare che lo stato di sorvegli potrebbe non essere il miglior modo per evitare che il tuo partner abusivo controlli i tuoi spostamenti e le tue conversazioni.
Questa guida vuole metterti a conoscenza delle possibilità e dei rischi così che tu prenda la decisione migliore in ogni situazione.
La cultura alla sicurezza non è una garanzia di sicurezza, ma riduce i possibili danni.
Può evitare che ti arrestino o salvare la tua vita o quelle delle persone accanto a te.

Questo testo è stato scritto all'inizio del 2022 e tradotto a inizio 2023 da alcuni anarchici e anarchiche che risiedono in europa, in nord america e la maggior parte di ciò che sarà scritto riteniamo sia più utile per le persone che condividono gli stessi spazi e lo stesso tempo.
Abbiamo intenzionalmente omesso la maggior parte delle considerazioni legali; considerato che anche se i nostri avversari non sono autorizzati a fare certe cose poi le fanno lo stesso.
Abbiamo preferito concentrarci su ciò che è tecnicamente possibile.
Riconoscendo e migliorando anche grazie a quelli che sono gli errori che non abbiamo visto anni fa (e nuovi errori disponibili in questo testo), ma non siamo in grado di predirre il futuro.
Quindi ti consigliamo di usare la tua conoscienza del contesto locale per adattare ogni dettaglio qui scritto alle minacce che affronterai direttamente.
