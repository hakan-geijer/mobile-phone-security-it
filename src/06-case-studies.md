<!-- vim: set spelllang=it : -->

# Casi di studio

Per rendere ciò che abbiamo discusso più concreto vi riportiamo qualche caso studio preso dalla nostra esperienza.
In alcuni di questi casi ci sono individui che hanno già un threat model dettagliato e alcuni che non ce l'hanno.
Alcuni sono basati su leggende metropolitane e altri su fatti verificabili o congetture verosimili.
Laddove ci sono degli errori sono analizzati.

## Caso 1: pianificare assemblee per un'azione semi-pubblica

### Scenario

Un collettivo sta preparando un'occupazione che deve rimanere segreta fino al momento in cui avviene, dopodiché sarà pubblicata sui social.
Si prepara per la maggior parte in assemblee di persona in uno spazio sociale.

### Premesse

Il collettivo dà per scontato che la polizia sia interessata a prevenire le occupazioni e che ogni persona possa essere sorvegliata.
Questa sorveglianza include, ma non si limita a, malware che lo Stato può mettere sul telefono di ogni persona.

### Contromisure

Per impedire allo Stato di usare il microfono dei telefoni per registrare le riunione, tutti i telefoni vengono messi in una scatola di plastica sigillata in un'altra stanza.

### Analisi

E' vero che i telefoni possono essere compromessi da malware ed è vero che metterli in un'altra stanza impedisce che i microfoni registrino una conversazione.
Però c'è un presupposto necessario per l'efficacia di mettere i telefoni in un'altra stanza, che può essere verificato iniziando a registrare con un telefono, metterlo nella scatola e iniziare una conversazione ad alta voce per vedere quanto si sente.
Se le voci sono anche lontanamente riconoscibili, i frammenti della conversazione si possono recuperare con un software di editing audio.
Se lo spazio non viene ripulito regolarmente da microfoni e altre cimici, la conversazione potrebbe comunque essere registrata.
Se il gruppo o altri che frequentano il posto sono sorvegliati da vicino, potrebbero esserci dei microfoni direzionali che registrano le conversazioni dagli edifici vicini.
Se alcuni individui sono sottoposti a sorveglianza passiva (metadati), la presenza ripetuta dello stesso gruppo di telefoni nello stesso posto dalle 19 alle 21 per qualche mercoledì di seguito può svelare che c'è stata una riunione e quali erano i partecipanti.

### Consigli

Se si tolgono i telefoni è meglio spegnerli anche.
Meglio se vengono messi in un posto rumoroso per minimizzare le possibilità che carpiscano i suoni della conversazione.
Se il gruppo ritiene di poter essere perseguito per la sola preparazione di un reato, meglio se lasciano i telefoni a casa o li spengono prima di spostarsi verso il luogo della riunione.
Ancor meglio non portarsi gli stessi telefoni durante l'azione.
Se si vuole un alto livello di sicurezza, la sorveglianza del luogo e dei telefoni si può ridurre ancora ritrovandosi fuori dagli spazi di movimento.
Se invece per convenienza il gruppo vuole trovarsi in un luogo centrale e conosciuto, all'inizio dell'assemblea bisognerebbe stabilire di parlare solo dell'azione in questione e non di alte cose illegali.

## Caso 2: Chiacchiere spiate

### Scenario

Alcune persone appartenenti a un gruppo di affinità sono in un parco a farsi i fatti loro senza organizzare niente.
Hanno i telefoni accesi ma per sicurezza hanno l'abitudine di non parlare di azioni passate o scambiarsi storie visto che possono contenere indizi incriminanti.

### Premesse

Questo gruppo dà per assodato che la polizia voglia solo ascoltare le loro conversazioni se riguardano attività illegali passate o future, mentre le loro discussioni quotidiane non sono interessanti.

### Contromisure

Il gruppo non prende nessuna contromisura per evitare che le chiacchiere siano spiate.

### Analisi

Se intenzionalmente nessuno parla di piani o azioni, ovviamente nessun microfono potrà mai sentire ciò che non viene detto.
Però le azioni non sono l'unica cosa per cui lo Stato ha interesse.
Gossip, drammi, fatti amorosi, legami e anche la propensione reciproca di persone o organizzazioni all'interno di un movimento sono un elemento di interesse.
Tutto questo aiuta lo Stato a farsi una mappatura sociale dettagliata.
Se lo Stato sospetta che un individuo fosse coinvolto in qualcosa su cui sta indagando e sa che quella persona aveva dei complici, usare queste mappe costruite su pezzi di conversazioni casuali può aiutare ad accorciare le fila dei sospetti o rivelare i membri di un gruppo di affinità.
Spiare le chiacchiere può rivelare allo Stato chi si sente escluso o risentito per tentare di farlo diventare un informatore.
Può sfruttare piccoli conflitti o soffiare sull'emotività per fomentare conflitti interni.

### Consigli

C'è un gap generazionale tra chi si organizzava già prima della diffusione dei cellulari e chi ha iniziato dopo la loro proliferazione.
C'è anche un altro gap tra chi si organizzava con i telefoni prima della diffusione degli smartphone e chi si orgaizza da sempre in un mondo in cui quasi tutti i loro contatti hanno uno smartphone.
Questo gap si nota dalla capacità di organizzarsi senza telefoni, ad esempio fissare luogo e ora di un appuntamento senza cambiamenti dell'ultimo minuto.
In più, chi si organizzava prima ha visto il cambiamento per cui pian piano ognuno aveva sempre un microfono con sé.

Come detto sopra, gli smartphone ci permettono di comunicare in modo istantaneo e avere informazioni illimitate a portata di mano, in ogni momento.
Tutto ciò a prezzo di nuove tecniche di sorveglianza.
Bisogna essere consapevoli che i telefoni in casa, in macchina e negli spazi sociali possono raccogliere informazioni di contorno sui gruppi.
Se consigliassimo di spegnere più spesso i telefoni, saremmo persone paranoiche o direbbero che è infattibile.
La cosiddetta democrazia liberale dà l'illusione che non viviamo in uno stato di polizia, eppure ci sono molti casi di gruppi innocui che vengono sorvegliati, per non parlare di quelli più radicali e attivi.

Non diciamo che non dovremmo mai avere il telefono dietro, ma vogliamo che tutti e tutte siano più consapevoli dell'impegno dello Stato nella sorveglianza e dell'utilità delle informazioni raccimolate da conversazioni qualunque.
Forse verrà il momento in cui la repressione si inasprirà e cominceremo a sentire di più la sua presenza.
Per prepararci a quei tempi e abituarci a resistere, il nostro consiglio è più moderato: alzare il livello di sicurezza ora.
Provate a organizzare eventi senza telefoni.
Quando vi fate i fatti vostri, andate a camminare in montagna, anche se vi vedete al pub, provate a fare in modo che
ognuno lasci i telefoni a casa.
Abituatevi alla loro assenza. Provate l'ebbrezza di non regalare dati allo Stato e sapere che solo i presenti possono sentire le vostre conversazioni.

## Caso 3: le occupazioni e i telefoni semplici

### Scenario

Un gruppo di persone vuole occupare un edificio abbandonato per attirare l'attenzione sulla speculazione edilizia e farlo diventare una casa per gli abitanti della zona che sono stati appena sfrattati.
Un gruppetto entra nel posto mentre gli altri stanno fuori e postano sui social.

### Premesse

Il gruppo di occupazione pensa che la polizia possa capire la loro identità guardando che telefoni stanno comunicando da dentro l'edificio, e anche se non verranno arrestati o perseguitate stavolta, questa informazione potrà essere usata contro di loro in futuro.

### Contromisure

Per ridurre il rischio di svelare la loro identità, il gruppo di occupazione ha scelto di non portare i telefoni personali.
Porteranno solo un telefono "pulito" per comunicare con il gruppo fuori e per evitare di rimanere isolati fino alla fine dell'azione.
Useranno una SIM senza nominativo per mantenere completo anonimato.

### Analisi

Il gruppo fa bene a non portarsi il telefono durante l'occupazione.
La polizia potrebbe scoprire le loro identità guardando quali telefoni si trovano dentro all'edificio.
Ma occhio al telefono "pulito".
Visto che alcuni del gruppo rimangono fuori a volto scoperto, l'identità del gruppo è in generale riconoscibile, anche se non si può sapere con esattezza chi è dentro l'edificio.
Se il telefono pulito è di qualcuno o è stato acceso a casa di
qualcuno, questa può essere usata come prova che quella persona si trovava
all'interno dell'edificio.[^ndt-1]
Attenzione anche al fatto che la polizia può usare un ricevitore di IMSI-catcher per leggere gli SMS in tempo reale e agire in base alle comunicazioni.

[^ndt-1]: *ndt*: questo vale anche in casi diversi dall'occupazione.

### Consigli

Il motivo per cui il gruppo di occupazione vuole portarsi solo un telefono è legittimo, ma devono usare un telefono con un account usa e getta usando Signal.
Questo account deve comunicare solo con un altro account anonimo per evitare di dare informazioni in caso venga confiscato.

## Caso 4: telefono semplice + Signal su computer

### Scenario

Ruben è militante in un gruppo di persone che credono sia sotto sorveglianza per le loro istanze antigovernative.
Per minimizzare le possibilità che la polizia lo localizzi, usa un telefono semplice con SIM quando è in giro.
Visto che alcuni discorsi con il suo gruppo sono più sensibili hanno bisogno di un sistema di messaggistica più sicuro e hanno scelto Signal.
Signal richiede di registrare un numero di telefono e genera la crittografia iniziale solo sulle app iOS o Android.
Per far funzionare Signal su PC ha usato la SIM card del suo telefono nello smartphone di un amico per impostare una coppia di codici iniziali da collegare al suo PC.
Dopo, Ruben fa logout sul telefono dell'amico e cancella Signal.

### Premesse

La scelta di Ruben di non portarsi dietro uno smartphone si basa sulla convinzione che gli smartphone siano più facilmente tracciabili dei telefoni normali.
Ruben presuppone anche che Signal sia più sicuro delle chiamate o degli SMS.

### Contromisure

La scelta di Ruben di usare un telefono semplice ha l'obiettivo di diminuire le possibilità di tracciamento.
La scelta di Signal è per evitare le intercettazione dei messaggi con altre persone.

### Analisi

La posizione di Ruben è tracciabile praticamente nella stessa maniera con un telefono semplice e con uno smartphone.
Le sue comunicazioni sono meno sicure perchè col telefono semplice non ha la possibilità di mandare o ricevere messaggi "di emergenza" con le persone di fiducia, e se lo fa, i suoi contatti saranno intercettati.

### Consigli

Ruben dovrebbe usare uno smartphone per le comunicazioni in generale.
Se a volte ha bisogno di nascondere dove si trova o non farsi intercettare, deve lasciare il telefono a casa.

## Caso 5: organizzarsi senza telefono

### Scenario

I membri di un gruppo di affinità fanno parte dei movimenti da tempo e sono ben conosciuti dallo Stato.
Stanno preparando qualcosa di grosso.
Non ne parlano tramite dispositivi elettronici, ma solo di persona.

### Premesse

Danno per scontato che lo Stato farà di tutto per prevenire la loro azione e soprattutto dopo per investigare.
Pensano sia possibile che ci siano dei malware dello Stato sui loro dispositivi.
Pensano che anche senza prove, saranno proprio loro i primi della lista dei sospetti per questa azione, quindi il loro livello di sicurezza deve essere impenetrabile.

### Contromisure

Vista la possibilità dei malware, ritengono i loro dispositivi non affidabili.
Vista la probabilità di essere oggetto di indagine, non parlano dell'azione a casa, in auto, negli spazi di movimento e in quelli dove bazzicano di solito.
Per ridurre i metadati delle comunicazioni tra loro, spengono i telefoni prima di arrivare al luogo di incontro e li riaccendono solo dopo essersene andate.

### Analisi

Il gruppo fa bene a pensare di essere sotto sorveglianza e fa bene a trattare i telefoni come spie.
Spegnere i telefoni diminuisce la possibilità di essere ascoltati da malware che si servono dei microfoni, e serve anche in una certa misura a nascondere la loro posizione durante gli incontri.
Ma l'assenza di queste informazioni può essere strana se confrontata con quella del loro utilizzo normale dei telefoni, e il fatto che tutti i loro telefoni spariscano contemporaneamenti nei pressi di uno stesso posto può essere un indizio per lo Stato.
Questo può essere motivo di ulteriore sorveglianza e portare, ad esempio all'istallazione di cimici in quel preciso posto---se usato più volte---o mandare uno sbirro in borghese nel bar dove si trovano.
In più, se un membro del gruppo viene arrestato e non dice niente negli interrogatori, comunque la polizia può guardare il suo telefono in cerca di anomalie.
Ad esempio: all'ora in cui questo telefono è stato spento, quali altri telefoni sono stati spenti nelle vicinanze?
E cosa facevano i telefoni degli altri sulla lista dei sospetti? Questo può rivelare il resto del gruppo di affinità o dare elementi per provare che proprio quelle persone erano le complici.
È possibile che la polizia non faccia queste operazioni, ma è meglio non lasciare traccia.

### Consigli

Visto che prevedono una sorveglianza mirata sulle loro attività, dovrebbero lasciare tutti i dispositivi elettronici a casa e scegliere per i loro incontri posti casuali, che siano molto isolati o molto rumorosi.

## Caso 6: Telefoni e manifestazioni di massa

### Scenario

Isa è un'attivista che partecipa alle grandi manifestazioni e anche se lei non è particolarmente radicale ha degli amici che lo sono, e di solito è al corrente di quel che fanno.
I fascisti hanno organizzato una marcia e Isa e alcuni amici si uniranno al gruppo che spera di bloccare il percorso che i fascisti vogliono fare.
Per stare in contatto con gli amici ed essere aggiornata sui blocchi o i cambi di percorso, Isa porta con sè il telefono che usa di solito (che è anche l'unico).

### Premesse

Isa non teme l'arresto perchè in situazioni del genere in passato, cioè quando una grande massa di gente blocca le strade, di solito la polizia cerca di arginarla o portarla lontano prima di far passare i fascisti.
Non pensa che se fosse mai arrestata guarderebbero il suo telefono, in modo legale o illegale.
Non si preoccupa nemmeno della sua localizzazione.

### Contromisure

Isa non prende contromisure per non far raccogliere dati sulla sua posizione o per non farsi sequestrare il telefono.

### Analisi

Nelle manifestazioni di massa la polizia può usare IMSI-catcher per vedere chi ha partecipato e tracciarne i profili.
In questi casi la posizione può essere usata per perseguire delle persone per eventuali scontri, anche se magari la denuncia non si tramuta in condanna.
Se Isa viene arrestata, cosa che comunque può succedere se i blocchi sono piccoli o se è tra le sfortunate che vengono acchiappate mentre si formano i blocchi, il suo telefono potrebbe essere ispezionato.
Da questo la polizia può scoprire i suoi legami e le attività dei suoi amici più radicali.
In sostanza questo può mettere in pericolo loro più che lei.

### Consigli

Anche se Isa non mette in conto l'arresto, dovrebbe essere più attenta col telefono.
Può mettersi d'accordo con gli amici di incontrarsi in un certo punto prima della manifestazione così può non portarsi dietro il telefono, o se proprio vogliono avere informazioni in tempo reale, solo una persona del gruppo dovrebbe portarsi il telefono.
Prestare attenzione al suo telefono può proteggere i suoi amici che partecipano ad azioni più rischiose.
Comunque, le probabilità che succeda una di queste cose è bassa, mentre è molto utile che lei si porti il telefono.
Questo lo rende un caso in cui “ci sta” che Isa si porti dietro il telefono... finchè non va male.

## Caso 7: organizzazione generica e comunicazioni

### Scenario

Un collettivo organizza proteste in modo legale e distribuisce volantini che promuovono alternative ecologiste allo status quo attuale, ad esempio la dieta vegana, la richiesta di finanziamenti per le piste ciclabili e un minor utilizzo dell'auto personale.
Usano una mailing list su un server radicale fidato.

### Premesse

Il collettivo presume che la polizia tenga genericamente sott'occhio gli attivisti, ma di non essere oggetto di particolare attenzione.
Sanno che ai troll piace molestare i "fricchettoni".
Sanno anche che ci sono organizzazioni ecologiste più militanti nella loro regione, e che le persone del proprio collettivo partecipano anche ad altri gruppi.

### Contromisure

Il collettivo vuole evitare le molestie, quindi tiene la mailing list privata e su invito.
Vogliono evitare tracciamenti dai grandi provider di email, quindi ne usano uno proprio.

### Analisi

Le mailing list sono molto usate perchè chiunque ha accesso alla mail, mentre ci sono molte app di messaggistica e non tutti usano le stesse.
Spesso le persone dicono di non avere memoria sul telefono per nuove app.
Alcuni membri del collettivo non hanno grandi capacità tecniche e non vogliono imparare ad usare nuove app, quindi a volte è inevitabile usare la mail.
Gli ecologisti di tutto il mondo, anche nelle cosiddette democrazie occidentali, sono sotto particolare sorveglianza anche se non propensi all'azione diretta.
Una mail su un provider privato può abbassare il rischio di intercettazioni, ma c'è sempre qualche anello debole in caso di richiesta dei dati dalle autorità giudiziarie.
Un grosso provider può ottemperare alla richiesta senza avvisare il collettivo, e anche se i server radicali che gestiscono il server per il collettivo lo farebbero sapere al collettivo senza problemi nonostante un obbligo a tacere, la polizia potrebbe aggirarli chiedendo direttamente alla compagnia che ospita il server di avere quei dati.
In più i server radicali devono essere competenti e affidabili poichè è possibile che piccoli gruppi potrebbero non avere le competenze tecniche dei grandi provider di email per tenere sicuro il server o far sapere se viene violato dai troll di Stato.

### Consigli

Se il problema è lo spazio sul telefono, le persone dovrebbero fare un backup di foto e video e cancellarle tutto sul telefono per fare spazio.
Questa è in generale una buona pratica per salvare dati in caso si perda o rompa il telefono.
Il collettivo dovrebbe spostare le comunicazoni su un'app di messaggistica con crittografia, ma se continuano a usare le email, dovrebbe essere solo per i dettagli più semplici come luogo e ora delle loro attività.
Organizzazione, dibattito interno, discussioni corpose dovrebbero stare fuori dalle email perchè queste sono tutte informazioni che danno allo
Stato una panoramica dettagliata del collettivo e possono essere usate contro di esso.

## Caso 8: i rave

### Scenario

Un collettivo organizza rave illegali all'aperto durante la pandemia di Covid.
Chiedono a chi partecipa di indossare le mascherine e pensano che questo sia un livello di sicurezza sufficiente contro la diffusione del virus.
La polizia ha dato il divieto assoluto di assembramenti (tranne che per il lavoro e altri ingranaggi della macchina capitalistica).

### Premesse

Lo Stato mette molte energie nell'evitare assembramenti (certo, solo quelli sgraditi), ma probabilmente non farà sforzi retroattivi per indagare su assembramenti passati.
Le forze dell'ordine hanno la possibilità di raccogliere dati sulla posizione dei telefoni in tempo reale e può usarli per scovare se centinaia di persone si sono radunate in qualche posto remoto.
La polizia ha informatori che seguono questo tipo di eventi, e ad alcune persone piace fare la spia se sentono cose non di proprio gradimento.

### Contromisure

Il rave non viene pubblicato sui social, e si chiede che le info siano solo inoltrate ad altri contatti con mezzi sicuri.
Nelle info è richiesto alle persone di mettere il telefono in modalità aerea quando si avvicinano vicino alla location.

### Analisi

Non pubblicizzare l'evento sul social ovviamente è un passaggio corretto per evitare che la polizia ne venga a conoscenza automaticamente.
Chiedere di diffondere le info a contatti fidati in modi sicuri è un ottimo modo per ridurre il rischio, ma basta che una sola persona mandi il messaggio solo con luogo e data per perdere l'avviso per strada.
Anche se il collettivo lo sa, è un rischio che ritiene accettabile.

### Consigli

C'è poco da fare per evitare che la gente arrivi col telefono acceso, e poco si può fare per assicurarsi che il messaggio-info rimanga su canali fidati e con un occhio alla sicurezza.
Questo è un grande problema nella cultura della sicurezza perchè la mancanza di un buon livello di sicurezza da parte di alcune persone può avere ripercussioni su tutto il gruppo, soprattutto dal momento che il beneficio individuale di tenere il telefono acceso e alto ma il rischio individuale è basso.
Chi organizza e porta la strumentazione è chi più probabilmente deve affrontare le conseguenze.
Infatti se i partecipanti si disperdono durante un blitz della polizia, probabilmente eviteranno le conseguenze legali.
La cosa migliore che il collettivo può fare è convincere le persone prima e durante il rave del fatto che le loro azioni possono essere responsabili delle sorti del rave per tutti e tutte.

## Caso 9: gestire un anello debole

### Scenario

Un gruppo di affinità si occupa di contrastare i nazi che creano problemi ad alcune persone della loro comunità.
In questo gruppo la regola è non portare il telefono alle azioni notturne.
Felix, uno dei membri più attivi, pensa che sia una paranoia eccessiva e si rifiuta di lasciare il suo a casa.

### Premesse

Il gruppo ritiene che lo Stato possa usare la posizione dei telefoni per indagini sulle sue attività.
I membri pensano anche che Felix metta in pericolo il gruppo portandosi dietro il telefono.

### Contromisure

Per evitare che Felix metta in pericolo tutti e tutte, il gruppo ha temporaneamente sospeso le attività finché non si troverà un accordo con Felix.

### Analisi

Le azioni di Felix mettono davvero a rischio il gruppo, ed è sensato non farlo partecipare alle azioni.
Se il gruppo stoppa del tutto le azioni, però, questo può nuocere alla comunità, e il rischio di essere arrestati per i telefoni può variare a seconda di come si muove la polizia nel paese o nella regione in questione.

### Consigli

Il gruppo di affinità può creare un sottogruppo di persone che sono d'accordo a non portarsi il telefono durante le azioni e continuare il proprio lavoro.
In parallelo, può fare un percorso con Felix per fargli capire come e perché portarsi il telefono crea un rischio non necessario.
Possono discutere con lui del fatto che il suo comportamento mette il resto del gruppo a disagio e che le conseguenze di questo non ricadono solo su di lui.
Il gruppo può riuscire a rimanere affine a Felix ma può aver bisogno di escluderlo dalle azioni più rischiose se si rifiuta di lasciare il telefono a casa.
